CC = clang
BINS = test
LDFLAGS = -lcmocka
RM = rm -f

SRC = $(wildcard src/*.c)
TEST = exercises/test.c
EXECISES = exercises/two-stack-queue.o
OBJS = $(SRC:.c=.o)

CFLAGS = -std=c99 -Isrc -Wall -Wno-unused-function -pipe

all: $(BINS)

$(BINS): $(SRC) $(OBJS) $(EXECISES)
	$(CC) $(CFLAGS) $(OBJS) $(EXECISES) $(LDFLAGS) $(TEST) -o $@

%.o: %.c
	$(CC) -c $< -o $@

clean:
	$(foreach c, $(BINS), $(RM) $(c);)
	$(RM) $(OBJS) $(EXECISES)
