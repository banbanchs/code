#ifndef QUEUE_H
#define QUEUE_H

#define MAX_QUEUE_SIZE 30

typedef int ElemType;
typedef struct Queue {
    ElemType seq[MAX_QUEUE_SIZE];
    int head;
    int tail;
    int length;
} queue_t;
int queue_empty(queue_t *Q);
int queue_full(queue_t *Q);
int queue_enqueue(queue_t *Q, ElemType val);
ElemType queue_dequeue(queue_t *Q);

#endif
