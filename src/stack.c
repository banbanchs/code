#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stack.h"

int stack_full(stack_t *s)
{
    if (s->top == MAX_STACK-1)
        return true;
    return false;
}

stack_t *stack_init()
{
    stack_t *s = malloc(sizeof(struct Stack));
    s->top = 0;
    return s;
}

int stack_empty(stack_t *s)
{
    if (s->top == 0)
        return true;
    return false;
}

int stack_push(stack_t *s, ElemType val)
{
    if (stack_full(s))
        return -1;
    s->array[s->top++] = val;
    return 0;
}

ElemType stack_pop(stack_t *s)
{
    if (stack_empty(s))
        return -1;
    return s->array[--s->top];
}
