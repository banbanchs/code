#include <stdio.h>
#include "queue.h"

int queue_empty(queue_t *Q)
{
    return Q->head == Q->tail;
}

int queue_full(queue_t *Q)
{
    return (Q->head == Q->tail+1);
}

int queue_enqueue(queue_t *Q, ElemType val)
{
    if (queue_full(Q))
        return -1;
    Q->seq[Q->tail] = val;
    if (Q->tail == Q->length)
        Q->tail = 0;
    else
        Q->tail++;
    return 0;
}

ElemType queue_dequeue(queue_t *Q)
{
    if (queue_empty(Q))
        return -1;
    ElemType val = Q->seq[Q->head];
    if (Q->head == Q->length)
        Q->head = 0;
    else
        Q->head--;
    return val;
}
