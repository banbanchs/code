#ifndef STACK_H
#define STACK_H

#define MAX_STACK 20

typedef int ElemType;
typedef struct Stack {
    ElemType array[MAX_STACK];
    int top;
} stack_t;

int stack_full(stack_t *s);
stack_t *stack_init();
int stack_empty(stack_t *s);
int stack_push(stack_t *s, ElemType val);
ElemType stack_pop(stack_t *s);

#endif
