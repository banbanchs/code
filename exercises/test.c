#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "two-stack-queue.h"

static void test_queue(void **state)
{
    tw_queue_t *Q = tw_queue_init();
    tw_queue_enqueue(Q, 9);
    tw_queue_enqueue(Q, 3);
    tw_queue_enqueue(Q, 1);
    assert_int_equal(tw_queue_dequeue(Q), 9);
    assert_int_equal(tw_queue_dequeue(Q), 3);
    assert_int_equal(tw_queue_dequeue(Q), 1);
    (void) state; /* unused */
}

int main(void)
{
    const UnitTest tests[] = {
        unit_test(test_queue),
    };
    return run_tests(tests);
}
