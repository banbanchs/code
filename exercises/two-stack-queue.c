#include <stdio.h>
#include <stdlib.h>
#include "two-stack-queue.h"

/*
 * 10.1-6  用两个栈实现一个队列
 *
 * 使用两个栈，一个用于ENQUEUE，另一个用于DEQUEUE
 *
 * **关键**：
 * 在出列时，如果`s_out`非空时，不用执行`reverse_queue`
 * 而是直接`stack_pop(s_out)`
 *
 */


tw_queue_t *tw_queue_init(void)
{
    tw_queue_t *Q = malloc(sizeof(struct twQueue));
    Q->s_in = stack_init();
    Q->s_out = stack_init();
    return Q;
}

// NEED more specific instruction to judge the queue is full
int tw_queue_full(tw_queue_t *Q)
{
    return stack_full(Q->s_in) || stack_full(Q->s_out);
}

int tw_queue_empty(tw_queue_t *Q)
{
    return stack_empty(Q->s_in) && stack_empty(Q->s_out);
}

int tw_queue_enqueue(tw_queue_t *Q, ElemType val)
{
    stack_push(Q->s_in, val);
    return 0;
}

void reverse_stack(stack_t *in, stack_t *out)
{
    while (!stack_empty(in) && !stack_full(out)) {
        stack_push(out, stack_pop(in));
    }
}

ElemType tw_queue_dequeue(tw_queue_t *Q)
{
    if (tw_queue_empty(Q))
        return -1;
    if (stack_empty(Q->s_out))
        reverse_stack(Q->s_in, Q->s_out);
    return stack_pop(Q->s_out);
}
