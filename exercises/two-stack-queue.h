#ifndef TWO_STACK_QUEUE_H
#define TWO_STACK_QUEUE_H
#include "../src/stack.h"

typedef struct twQueue {
    stack_t *s_in;
    stack_t *s_out;
} tw_queue_t;

tw_queue_t *tw_queue_init(void);
int tw_queue_full(tw_queue_t *Q);
int tw_queue_empty(tw_queue_t *Q);
int tw_queue_enqueue(tw_queue_t *Q, ElemType val);
void reverse_stack(stack_t *in, stack_t *out);
ElemType tw_queue_dequeue(tw_queue_t *Q);


#endif
